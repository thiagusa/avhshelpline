import 'dart:convert';
import 'dart:io';

import 'package:avhshelpline/homePage/homePage.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/formWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  BuildContext? baseContext;

  SignUp({
    Key? key,
    this.baseContext,
  }) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final nameController = TextEditingController();
  final phoneNumberController = TextEditingController();
  final addressController = TextEditingController();
  final emailController = TextEditingController();
  final talukController = TextEditingController();
  final villageController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode nameFocusNode = new FocusNode();
  final FocusNode phoneNumberFocusNode = new FocusNode();
  final FocusNode addressFocusNode = new FocusNode();
  final FocusNode emailFocusNode = new FocusNode();
  final FocusNode talukFocusNode = new FocusNode();
  final FocusNode villageFocusNode = new FocusNode();

  bool isLoading = false;

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    print("SignUp Page");
  }

  void signUpClick() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage()));
    // if (_formKey.currentState.validate()) {
    //   if (schoolNameController.text.trim() != "" &&
    //       teacherNameController.text.trim() != "" &&
    //       phoneNumberController.text.trim() != "" &&
    //       addressController.text.trim() != "" &&
    //       emailController.text.trim() != "" &&
    //       noOfStudentsController.text.trim() != "" &&
    //       smartBoardsController.text.trim() != "") {
    //     Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //           builder: (context) => SignUpNextPage(
    //             school_name: schoolNameController.text,
    //             teacher_name: teacherNameController.text,
    //             phone_no: phoneNumberController.text,
    //             address: addressController.text,
    //             email: emailController.text,
    //             no_of_smartboards: smartBoardsController.text,
    //             no_of_students: noOfStudentsController.text,
    //           )),
    //     ).then((signUpStatus) {
    //       if (signUpStatus != null) {
    //         SignUpDataStorage parsedData = signUpStatus as SignUpDataStorage;
    //         Navigator.of(context).pop(parsedData);
    //       }
    //     });
    //   } else {
    //     showInSnackBar(_scaffoldKey, "Enter all fields");
    //   }
    // }
  }

  Widget uiLayout() {
    return Container(
      child: Column(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Column(
              children: <Widget>[
                new Form(
                    key: _formKey,
                    child: new Column(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 25.0),
                          child: new Image.asset(
                            'assets/sample.png',
                            scale: 5,
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 100.0),
                          child: BoxEditText(
                              fieldName: "Name",
                              textEditingController: nameController,
                              labelColor: black,
                              textColor: black,
                              focusNode: nameFocusNode,
                              focusTo: emailFocusNode,
                              iconColor: black,
                              textPadding: 5.0,
                              fontSize: 15,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isCaps: true,
                              isMobile: false,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Name is missing';
                                }
                              }),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Email",
                              textEditingController: emailController,
                              labelColor: black,
                              textColor: black,
                              focusNode: emailFocusNode,
                              focusTo: phoneNumberFocusNode,
                              iconColor: black,
                              textPadding: 5.0,
                              fontSize: 15,
                              isEmail: true,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isMobile: false,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Email is missing';
                                } else {
                                  // Pattern pattern =
                                  //     r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                                  if (!regex.hasMatch(value))
                                    return 'Enter a valid Email';
                                  else
                                    return null;
                                }
                              }),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Phone number",
                              textEditingController: phoneNumberController,
                              labelColor: black,
                              textColor: black,
                              focusNode: phoneNumberFocusNode,
                              focusTo: addressFocusNode,
                              iconColor: black,
                              textPadding: 5.0,
                              fontSize: 15,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isMobile: true,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Phone number is missing';
                                } else if (value.length != 10) {
                                  return 'Mobile Number must be of 10 digit';
                                } else
                                  return null;
                              }),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Address",
                              textEditingController: addressController,
                              labelColor: black,
                              textColor: black,
                              focusNode: addressFocusNode,
                              focusTo: talukFocusNode,
                              iconColor: black,
                              textPadding: 5.0,
                              fontSize: 15,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isNewLine: true,
                              isCaps: true,
                              isMobile: false,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Taluk is missing';
                                }
                              }),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Taluk Name",
                              textEditingController: talukController,
                              labelColor: black,
                              textColor: black,
                              focusNode: talukFocusNode,
                              focusTo: villageFocusNode,
                              iconColor: black,
                              textPadding: 5.0,
                              fontSize: 15,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isCaps: true,
                              isMobile: false,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Taluk Name is missing';
                                }
                              }),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Village Name",
                              textEditingController: villageController,
                              labelColor: black,
                              textColor: black,
                              focusNode: villageFocusNode,
                              focusTo: null,
                              iconColor: black,
                              textPadding: 5.0,
                              fontSize: 15,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isCaps: true,
                              isMobile: false,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Village Name is missing';
                                }
                              }),
                        ),
                      ],
                    )),
                new Padding(
                  padding: EdgeInsets.only(
                      left: 40.0, right: 70.0, bottom: 30.0, top: 40.0),
                  child: ButtonWidget(
                    text: "Sign Up",
                    isBold: true,
                    textColor: white,
                    bgColor: blueBlackColor,
                    buttonCallback: signUpClick,
                    context: context,
                    roundRadius: 25.0,
                    isFullWidth: false,
                    isAutoWidth: true,
                    fontsize: 20,
                    padding: new EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        body: new Container(
          width: double.infinity,
          height: double.infinity,
          decoration: new BoxDecoration(
              image: DecorationImage(
                  image: ExactAssetImage('assets/Joopop_assets/Login_bg.jpg'),
                  fit: BoxFit.fill,
                  alignment: Alignment.topCenter)),
          child: new Stack(
            children: <Widget>[
              !isLoading
                  ? new ListView(
                shrinkWrap: false,
                physics: const AlwaysScrollableScrollPhysics(),
                children: <Widget>[uiLayout()],
              )
                  : centeredCircularProgressIndicator(color: primaryColor)
            ],
          ),
        ));
  }
}
