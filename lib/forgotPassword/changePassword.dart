import 'dart:convert';
import 'dart:io';

import 'package:avhshelpline/login/usersLogin.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:avhshelpline/util/formWidgets.dart';
import 'package:avhshelpline/util/network.dart';
import 'package:avhshelpline/util/scaffoldWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChangePassword extends StatefulWidget {
  BuildContext? baseContext;
  String? email;
  String? otp;

  ChangePassword({Key? key, this.baseContext, this.email, this.otp})
      : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final confirmPasswordController = TextEditingController();
  final passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode confirmPasswordFocusNode = new FocusNode();
  final FocusNode passwordFocusNode = new FocusNode();

  bool isLoading = false;

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    print("Change Password Page");
  }

  // Future changePassword() async {
  //   await checkNetworkStatus().then((isNetworkAvailabe) async {
  //     if (isNetworkAvailabe) {
  //       if (_formKey.currentState.validate()) {
  //         if (confirmPasswordController.text.trim() != "" &&
  //             passwordController.text.trim() != "") {
  //           setState(() {
  //             isLoading = !isLoading;
  //           });
  //
  //           changePasswordApi(
  //               email: widget.email,
  //               new_password: passwordController.text.trim(),
  //               auth_code: otp)
  //               .then((result) async {
  //             if (result.status_code == "200") {
  //               await save_string_prefs(
  //                   "user_info_by_login", json.encode(result));
  //               isLoading = !isLoading;
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(
  //                     builder: (context) => AccessCode(
  //                         catId: "", catagoryName: "", isTrial: false)),
  //               );
  //               ;
  //             } else {
  //               setState(() {
  //                 isLoading = !isLoading;
  //                 showInSnackBar(_scaffoldKey, "Incorrect credentials");
  //               });
  //             }
  //           }).catchError((onError) {
  //             setState(() {
  //               isLoading = !isLoading;
  //               showInSnackBar(_scaffoldKey, onError.toString());
  //             });
  //           });
  //         } else {
  //           showInSnackBar(_scaffoldKey, "Fields empty");
  //         }
  //       }
  //     } else {
  //       showInSnackBar(_scaffoldKey, "Network unavailable");
  //     }
  //   });
  // }

 void changePassword(){
   Navigator.pushReplacement(
     context,
     MaterialPageRoute(builder: (context) => UserLogin()),
   );
 }

  Widget uiLayout() {
    return Container(
      child: Column(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Column(
              children: <Widget>[
                new Form(
                    key: _formKey,
                    child: new Column(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 25.0),
                          child: new Image.asset(
                              'assets/sample.png',
                          scale: 5,),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 100.0),
                          child: BoxEditText(
                              fieldName: "Password",
                              textEditingController: passwordController,
                              labelColor: black,
                              textColor: black,
                              focusNode: passwordFocusNode,
                              focusTo: confirmPasswordFocusNode,
                              iconColor: black,
                              textPadding: 5.0,
                            //  sufixIcon: JoopopSchool_Icons.password,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isMobile: false,
                              isPassword: true,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Password is missing';
                                }
                              }),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Confirm Password",
                              textEditingController: confirmPasswordController,
                              labelColor: black,
                              textColor: black,
                              focusNode: confirmPasswordFocusNode,
                              focusTo: null,
                              iconColor: black,
                              textPadding: 5.0,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                             // sufixIcon: JoopopSchool_Icons.password,
                              isMobile: false,
                              isPassword: true,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Confirm Password is missing';
                                }
                              }),
                        ),
                      ],
                    )),
                new Padding(
                  padding: EdgeInsets.only(
                      left: 40.0, right: 70.0, bottom: 30.0, top: 60.0),
                  child: ButtonWidget(
                    text: "Confirm",
                    isBold: true,
                    textColor: white,
                    bgColor: black,
                    buttonCallback: changePassword,
                    context: context,
                    roundRadius: 25.0,
                    isFullWidth: false,
                    isAutoWidth: true,
                    fontsize: 20,
                    padding: new EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    calculateFontSizeWithDeviceSize(context);
    return new Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 0.0,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                "Confirm Password",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
              ),
            ],
          ),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),

        body: new Container(
          width: double.infinity,
          height: double.infinity,
          // decoration: new BoxDecoration(
          //     image: DecorationImage(
          //         image: ExactAssetImage('assets/Joopop_assets/Login_bg.jpg'),
          //         fit: BoxFit.fill,
          //         alignment: Alignment.topCenter)),
          child: new Stack(
            children: <Widget>[
              !isLoading
                  ? new ListView(
                shrinkWrap: false,
                physics: const AlwaysScrollableScrollPhysics(),
                children: <Widget>[uiLayout()],
              )
                  : centeredCircularProgressIndicator(color: primaryColor)
            ],
          ),
        ));
  }
}
