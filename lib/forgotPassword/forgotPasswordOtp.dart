import 'dart:convert';
import 'dart:io';

import 'package:avhshelpline/forgotPassword/changePassword.dart';
import 'package:avhshelpline/forgotPassword/forgotPassword.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:avhshelpline/util/formWidgets.dart';
import 'package:avhshelpline/util/scaffoldWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ForgotPasswordOTP extends StatefulWidget {
  BuildContext? baseContext;
  String? email;

  ForgotPasswordOTP({Key? key, this.baseContext, this.email}) : super(key: key);

  @override
  _ForgotPasswordOTPState createState() => _ForgotPasswordOTPState();
}

class _ForgotPasswordOTPState extends State<ForgotPasswordOTP> {
  final pinController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode pinFocusNode = new FocusNode();

  bool isLoading = false;

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    print("Forgot Password Page");
  }
  void forgotPasswordOTP() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => ChangePassword(
            email: widget.email,
            otp: otp,
          )),
    );
  }
  // void forgotPasswordOTP() {
  //   if (pinController.text.trim() != "") {
  //     String pin = pinController.text.trim();
  //
  //     if (otp == pin) {
  //       Navigator.push(
  //         context,
  //         MaterialPageRoute(
  //             builder: (context) => ChangePassword(
  //               email: widget.email,
  //               otp: otp,
  //             )),
  //       );
  //     } else {
  //       showInSnackBar(_scaffoldKey, "Enter valid code");
  //     }
  //   } else {
  //     showInSnackBar(_scaffoldKey, "Enter code");
  //   }
  // }

  Widget uiLayout() {
    return Container(
      child: Column(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Column(
              children: <Widget>[
                new Form(
                    key: _formKey,
                    child: new Column(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 25.0),
                          child: new Image.asset(
                              'assets/sample.png',
                          scale: 5,),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 100.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[new Text('Access code',
                            style: new TextStyle(
                              fontSize: 20
                            ),)],
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: new Column(
                            children: <Widget>[
                              new Padding(
                                padding: EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 5.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Flexible(
                                      child: BoxEditText(
                                        textEditingController: pinController,
                                        labelColor: black,
                                        textColor: black,
                                        fontSize: 20,
                                        focusNode: pinFocusNode,
                                        focusTo: null,
                                        textPadding: 5.0,
                                        isEmail: false,
                                        isBox: false,
                                        isLabel: false,
                                        lineColor: black,
                                        lengthSize: 6,
                                        isMobile: false,
                                        isPassword: false,
                                        validator: (value) {  },
                                      ),
                                    ),
                                  ],
                                ),
                                // end PinEntryTextField(),
                              ),
                              new Padding(padding:
                              EdgeInsets.only(
                                  top: 10.0, bottom: 10.0, right: 20.0
                              ),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text('*Access code has been sent to your email id. \n  Please check spam folder also.',
                                      style: new TextStyle(
                                          color: black,
                                          fontSize: 12
                                      ),),

                                  ],
                                ),),
                            ],
                          ),
                        ),
                      ],
                    )),
                new Padding(
                  padding: EdgeInsets.only(
                      left: 40.0, right: 70.0, bottom: 30.0, top: 30.0),
                  child: ButtonWidget(
                    text: "Next",
                    isBold: true,
                    textColor: white,
                    bgColor: blueBlackColor,
                    buttonCallback: forgotPasswordOTP,
                    context: context,
                    roundRadius: 25.0,
                    isFullWidth: false,
                    isAutoWidth: true,
                    fontsize: 20,
                    padding: new EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    calculateFontSizeWithDeviceSize(context);
    return new Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 0.0,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                "Forgot Password",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
              ),
            ],
          ),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),
        body: new Container(
          width: double.infinity,
          height: double.infinity,
          decoration: new BoxDecoration(
              image: DecorationImage(
                  image: ExactAssetImage('assets/Joopop_assets/Login_bg.jpg'),
                  fit: BoxFit.fill,
                  alignment: Alignment.topCenter)),
          child: new Stack(
            children: <Widget>[
              !isLoading
                  ? new ListView(
                shrinkWrap: false,
                physics: const AlwaysScrollableScrollPhysics(),
                children: <Widget>[uiLayout()],
              )
                  : centeredCircularProgressIndicator(color: primaryColor)
            ],
          ),
        ));
  }
}