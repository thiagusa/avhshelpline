import 'dart:convert';
import 'dart:io';

import 'package:avhshelpline/forgotPassword/forgotPasswordOtp.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:avhshelpline/util/formWidgets.dart';
import 'package:avhshelpline/util/network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

String? otp;

class ForgotPassword extends StatefulWidget {
  BuildContext? baseContext;

  ForgotPassword({
    Key? key,
    this.baseContext,
  }) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final emailController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode emailFocusNode = new FocusNode();

  bool isLoading = false;

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    print("Forgot Password Page");
  }

  // Future forgotPasswordEmail() async {
  //   await checkNetworkStatus().then((isNetworkAvailabe) async {
  //     if (isNetworkAvailabe) {
  //       if (_formKey.currentState.validate()) {
  //         if (emailController.text.trim() != "") {
  //           setState(() {
  //             isLoading = !isLoading;
  //           });
  //
  //           forgotPasswordApi(
  //             email: emailController.text.trim(),
  //           ).then((result) async {
  //             if (result.status_code == "200") {
  //               otp = result.data.auth_code.toString().trim();
  //               await save_string_prefs(
  //                   "user_info_by_login", json.encode(result));
  //               isLoading = !isLoading;
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(
  //                     builder: (context) => ForgotPasswordOTP(
  //                       email: emailController.text,
  //                     )),
  //               );
  //             } else {
  //               setState(() {
  //                 isLoading = !isLoading;
  //                 showInSnackBar(_scaffoldKey, "Incorrect credentials");
  //               });
  //             }
  //           }).catchError((onError) {
  //             setState(() {
  //               isLoading = !isLoading;
  //               showInSnackBar(_scaffoldKey, onError.toString());
  //             });
  //           });
  //         } else {
  //           showInSnackBar(_scaffoldKey, "Fields empty");
  //         }
  //       }
  //     } else {
  //       showInSnackBar(_scaffoldKey, "Network unavailable");
  //     }
  //   });
  // }

  void forgotPassword() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => ForgotPasswordOTP()),
    );
  }

  Widget uiLayout() {
    return Container(
      child: Column(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Column(
              children: <Widget>[
                new Form(
                    key: _formKey,
                    child: new Column(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 25.0),
                          child: new Image.asset(
                            'assets/sample.png',
                            scale: 5,
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 100.0),
                          child: BoxEditText(
                              fieldName: "Email / Mobile Number",
                              textEditingController: emailController,
                              labelColor: black,
                              textColor: black,
                              focusNode: emailFocusNode,
                              focusTo: null,
                              iconColor: black,
                              textPadding: 5.0,
                              //sufixIcon: JoopopSchool_Icons.mail,
                              isEmail: true,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isMobile: true,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Email / Mobile Number is missing';
                                } else {
                                  // Pattern pattern =
                                  //     r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regex = new RegExp(
                                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                                  if (!regex.hasMatch(value))
                                    return 'Enter a valid Email';
                                  else
                                    return null;
                                }
                              }),
                        ),
                      ],
                    )),
                new Padding(
                  padding: EdgeInsets.only(
                      left: 40.0, right: 70.0, bottom: 30.0, top: 60.0),
                  child: ButtonWidget(
                    text: "Next",
                    isBold: true,
                    textColor: white,
                    bgColor: blueBlackColor,
                    buttonCallback: forgotPassword,
                    context: context,
                    roundRadius: 25.0,
                    isFullWidth: false,
                    isAutoWidth: true,
                    fontsize: 20,
                    padding: new EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    calculateFontSizeWithDeviceSize(context);
    return new Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 0.0,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                "Forgot Password",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
              ),
            ],
          ),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),
        body: new Container(
          width: double.infinity,
          height: double.infinity,
          // decoration: new BoxDecoration(
          //     image: DecorationImage(
          //         image: ExactAssetImage('assets/Joopop_assets/Login_bg.jpg'),
          //         fit: BoxFit.fill,
          //         alignment: Alignment.topCenter)),
          child: new Stack(
            children: <Widget>[
              !isLoading
                  ? new ListView(
                      shrinkWrap: false,
                      physics: const AlwaysScrollableScrollPhysics(),
                      children: <Widget>[uiLayout()],
                    )
                  : centeredCircularProgressIndicator(color: primaryColor)
            ],
          ),
        ));
  }
}
