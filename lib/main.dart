// @dart=2.9
import 'package:avhshelpline/login/loginType.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:flutter/material.dart';
//import 'package:avhs/Aboutus/Aboutus.dart';

import 'homePage/homePage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:avhshelpline/login/loginscreen.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}




//void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.pink,
        primaryColor: primaryColor,
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle:
            new TextStyle(color: Colors.grey, fontSize: 15.0) //labelStyle
        ), //InputDecoration
      ),
      home: new LoginType(), //ThemeData
    ); //Material App
  }
}