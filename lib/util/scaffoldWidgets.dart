import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/formWidgets.dart';
import 'package:flutter/material.dart';

void showInSnackBar(GlobalKey<ScaffoldState> scaffoldKey, String value) {
  scaffoldKey.currentState!
      .showSnackBar(new SnackBar(content: new Text(value)));
}

class TabWidget extends StatefulWidget {
  int tabCount;
  String appBarTitle;
  List<Tab> tabElements;
  List<Widget> tabPages;
  bool isTabScrollable;
  Color? indicatorColor;
  TabController? tabController;
  Color? unselectedLabelColor;
  GlobalKey<ScaffoldState>? scaffoldKey;

  TabWidget(
      {Key? key,
      this.isTabScrollable: true,
      required this.tabCount,
      required this.appBarTitle,
      required this.tabElements,
      required this.tabPages,
      this.unselectedLabelColor,
      this.indicatorColor,
      this.scaffoldKey,
      this.tabController})
      : super(key: key);

  @override
  _TabWidgetState createState() => _TabWidgetState();
}

class _TabWidgetState extends State<TabWidget> {
  Widget uiLayout() {
    return DefaultTabController(
      length: widget.tabCount,
      child: Scaffold(
          appBar: new AppBar(
            elevation: 0.0,
            centerTitle: true,
            title: Text(widget.appBarTitle),
            bottom: TabBar(
              controller: widget.tabController,
              isScrollable: widget.isTabScrollable,
              indicatorColor: widget.indicatorColor,
              tabs: widget.tabElements,
              unselectedLabelColor: widget.unselectedLabelColor,
            ),
          ),
          key: widget.scaffoldKey,
          body: new Stack(children: [
            Image.asset(
              "assets/bg.png",
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
            TabBarView(
              controller: widget.tabController,
              children: widget.tabPages,
            ),
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return uiLayout();
  }
}

class TabWithoutScaffoldWidget extends StatefulWidget {
  int tabCount;
  List<Tab> tabElements;
  List<Widget> tabPages;
  bool isTabScrollable;
  Color? indicatorColor;
  TabController tabController;
  Color? unselectedLabelColor;
  GlobalKey<ScaffoldState>? scaffoldKey;

  TabWithoutScaffoldWidget(
      {Key? key,
      this.isTabScrollable: true,
      required this.tabCount,
      required this.tabElements,
      required this.tabPages,
      this.unselectedLabelColor,
      this.indicatorColor,
      this.scaffoldKey,
      required this.tabController})
      : super(key: key);

  @override
  _TabWithoutScaffoldWidgetState createState() =>
      _TabWithoutScaffoldWidgetState();
}

class _TabWithoutScaffoldWidgetState extends State<TabWithoutScaffoldWidget> {
  Widget uiLayout() {
    return DefaultTabController(
      length: widget.tabCount,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.tabElements != null
              ? TabBar(
                  controller: widget.tabController,
                  isScrollable: widget.isTabScrollable,
                  indicatorColor: widget.indicatorColor,
                  tabs: widget.tabElements,
                  unselectedLabelColor: widget.unselectedLabelColor,
                )
              : new Container(),
          new Expanded(
              child: TabBarView(
            controller: widget.tabController,
            children: widget.tabPages,
          ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return uiLayout();
  }
}

class ListWithDivider extends StatefulWidget {
  double borderWidth;
  String title;
  Color borderColor;
  IconData? icon;
  Function action;
  bool isNavTile;
  String subtitle;
  Color? textColor;
  Color? subColor;
  double? titleSize;
  double? subSize;
  Color? iconColor;

  ListWithDivider(
      {Key? key,
      required this.title,
      required this.borderWidth,
      this.icon,
      required this.action,
      required this.borderColor,
      required this.subtitle,
      this.iconColor,
      required this.isNavTile,
      this.textColor,
      this.subColor,
      this.subSize,
      this.titleSize})
      : super(key: key);

  @override
  _ListWithDividerState createState() => _ListWithDividerState();
}

class _ListWithDividerState extends State<ListWithDivider> {
  Widget uiLayout() {
    return new Container(
        child: ListTile(
          title: widget.title != null
              ? Text(
                  widget.title,
                  style: new TextStyle(
                      color: widget.textColor ?? white,
                      fontSize: widget.titleSize ?? null),
                )
              : null,
          leading: widget.icon != null
              ? new Icon(
                  widget.icon,
                  color: widget.iconColor ?? white,
                )
              : null,
          // subtitle: widget.subtitle != null
          //     ? Text(widget.subtitle,
          //         style: new TextStyle(
          //             color: widget.subColor ?? white,
          //             fontSize: widget.subSize ?? 1))
          //     : null,
          onTap: () {
            if (widget.isNavTile) Navigator.of(context).pop();
            if (widget.action != null) widget.action();
          },
        ),
        decoration: new BoxDecoration(
            border: new Border(
                bottom: new BorderSide(
                    color: widget.borderColor, width: widget.borderWidth))));
  }

  @override
  Widget build(BuildContext context) {
    return uiLayout();
  }
}

class BottomTabWidget extends StatefulWidget {
  int currentIndex;
  Function(int) onTabTapped;
  List<BottomNavigationBarItem> tabElements;

  BottomTabWidget(
      {Key? key,
      required this.currentIndex,
      required this.tabElements,
      required this.onTabTapped})
      : super(key: key);

  @override
  _BottomTabWidgetState createState() => _BottomTabWidgetState();
}

class _BottomTabWidgetState extends State<BottomTabWidget> {
  Widget uiLayout() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.shifting,
      onTap: widget.onTabTapped,
      currentIndex: widget.currentIndex,
      items: widget.tabElements,
    );
  }

  @override
  Widget build(BuildContext context) {
    return uiLayout();
  }
}

dialogue(
    {required BuildContext context,
    required String alertBoxTitle,
    Color? titleBarColor,
    required bool isYesNO,
    required String msg,
    required Color textColor,
    required String positiveText,
    required String negativeText,
    required Function positiveCallBack,
    required Function negativeCallBack,
    bool? isDialogueDismisable,
    bool? negativeCallbackNeedsParam,
    bool? positiveCallbackNeedsParam,
    required Color msgTextColor,
    required Color negativeButtonTextColor,
    required Function callBack,
    required Color positiveButtonTextColor,
    required Color negativeButtonBgColor,
    required Color positiveButtonBgColor}) {
  return showDialog(
    context: context,
    barrierDismissible: isDialogueDismisable ?? true, // user must tap button!
    builder: (BuildContext context) {
      return new AlertDialog(
        //title: null,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        contentPadding: EdgeInsets.all(0.0),
        content: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Expanded(
                    child: new Container(
                  //width: double.infinity,
                  decoration: new BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0)),
                    color: titleBarColor,
                  ),
                  child: new Padding(
                    padding: EdgeInsets.all(15.0),
                    child: new Text(
                      alertBoxTitle,
                      style: new TextStyle(fontSize: 17.0, color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ))
              ],
            ),
            DialogueContent(
              isYesNO: isYesNO,
              msg: msg,
              msgTextColor: msgTextColor,
              textColor: textColor,
              context: context,
              positiveText: positiveText,
              negativeText: negativeText,
              positiveCallBack: positiveCallBack,
              negativeCallBack: negativeCallBack,
              negativeButtonTextColor: negativeButtonTextColor,
              negativeButtonBgColor: negativeButtonBgColor,
              positiveButtonBgColor: positiveButtonBgColor,
              positiveButtonTextColor: positiveButtonTextColor,
              callBack: callBack,
            )
          ],
        ),
      );
    },
  );
}

class DialogueContent extends StatefulWidget {
  Function callBack;
  bool isYesNO;
  String msg;
  String positiveText;
  String negativeText;
  Color? textColor;
  Color? msgTextColor;
  BuildContext context;
  Function? positiveCallBack;
  Function? negativeCallBack;
  Color? negativeButtonTextColor;
  Color? positiveButtonTextColor;
  Color? negativeButtonBgColor;
  Color? positiveButtonBgColor;

  DialogueContent({
    Key? key,
    required this.callBack,
    required this.isYesNO,
    required this.msg,
    this.msgTextColor,
    this.textColor,
    this.negativeButtonBgColor,
    this.positiveButtonBgColor,
    required this.context,
    required this.positiveText,
    this.negativeButtonTextColor,
    this.positiveButtonTextColor,
    required this.negativeText,
    this.negativeCallBack,
    this.positiveCallBack,
  }) : super(key: key);

  @override
  _DialogueContentState createState() => _DialogueContentState();
}

class _DialogueContentState extends State<DialogueContent> {
  void popBack() {
    Navigator.pop(context);
  }

  Widget uiLayout() {
    return new Container(
      color: Colors.transparent,
      child: new Column(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(
                left: 10.0, top: 30.0, right: 10.0, bottom: 30.0),
            child: new Text(
              widget.msg,
              style: new TextStyle(color: widget.msgTextColor, fontSize: 13.0),
              textAlign: TextAlign.justify,
            ),
          ),
          new Padding(
            padding:
                EdgeInsets.only(left: 10.0, top: 0, right: 10.0, bottom: 15.0),
            child: new Row(
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                widget.isYesNO
                    ? new Expanded(
                        child: new ButtonWidget(
                        text: widget.negativeText,
                        textColor: widget.negativeButtonTextColor,
                        bgColor: widget.negativeButtonBgColor,
                        context: widget.context,
                        buttonCallback: widget.negativeCallBack ?? popBack,
                        isFullWidth: false,
                        roundRadius: 10.0,
                       // callBack: widget.callBack,
                      ))
                    : new Container(),
                widget.isYesNO
                    ? new SizedBox(
                        width: 10.0,
                      )
                    : new Container(),
                new Expanded(
                    child: new ButtonWidget(
                  text: widget.positiveText,
                  textColor: widget.positiveButtonTextColor,
                  bgColor: widget.positiveButtonBgColor,
                  context: widget.context,
                  buttonCallback: widget.positiveCallBack ?? popBack,
                  isFullWidth: false,
                  roundRadius: 10.0,
                  //callBack: widget.callBack,
                )),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return uiLayout();
  }
}

class ButtonMap {
  String actionName;
  Function() buttonAction;

  ButtonMap({required this.actionName, required this.buttonAction});
}

class CustomDialogue extends StatefulWidget {
  bool isSimpleDialogue;
  String message;

  //List<ButtonMap> buttonList;

  CustomDialogue({
    Key? key,
    this.isSimpleDialogue: true,
    required this.message,
  }) : super(key: key);

  @override
  _CustomDialogueState createState() => _CustomDialogueState();
}

class _CustomDialogueState extends State<CustomDialogue> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: SingleChildScrollView(
        child: widget.isSimpleDialogue
            ? new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        left: 0.0, right: 0.0, top: 0.0, bottom: 5.0),
                    child: Text(
                      widget.message,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              )
            : new Container(),
      ),
    );
  }
}
