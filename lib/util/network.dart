import 'dart:async';
import 'dart:io';

Future<bool> checkNetworkStatus() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return true;
  } on SocketException catch (_) {
    return false;
  }
}
