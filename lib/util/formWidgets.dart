import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Widget centeredCircularProgressIndicator({Color? color}) {
  return new Center(
    child: new CircularProgressIndicator(
      valueColor: new AlwaysStoppedAnimation<Color>(color ?? white),
    ),
  );
}

class BoxEditText extends StatelessWidget {
  String? fieldName;
  FocusNode? focusNode;
  bool isEmail, isPassword, enabled, isMobile;
  String? hint;
  IconData? prefixIcon;
  Widget? prefixIconWidget;
  String? prefixImagePath;
  IconData? sufixIcon;
  String? sufixImagePath;
  TextEditingController? textEditingController;
  String? Function(String?) validator;
  //void Function(String?) onSaved;
  FocusNode? focusTo;
  bool isBox;
  bool isLabel;
  double textPadding;
  Color? lineColor;
  Color? labelColor;
  Color? textColor;
  Color? iconColor;
  bool isDone;
  bool isCaps;
  bool isMaxLine;
  bool isNewLine;
  bool isDate;
  Widget? child;
  double? radius;
  double? fontSize;
  String? initialValue;
  int? lengthSize;
  String? searchName;
  String? prefixText;
  Brightness? numberOnly;
  Color? errorColor;
  Widget? prefixWidget;
  Widget? sufixWidget;
  bool isNumber;
  bool isOnlyLetters;

  BoxEditText(
      {this.focusNode,
      this.sufixIcon,
      this.lengthSize,
      this.initialValue,
      this.fontSize,
      this.isNumber: false,
      this.searchName,
      this.prefixText,
      this.sufixImagePath,
      this.fieldName,
      this.hint,
      this.prefixWidget,
      this.sufixWidget,
      this.child,
      this.prefixIconWidget,
      this.radius,
      this.numberOnly,
      this.isDone: true,
      this.isDate: false,
      this.isNewLine: false,
      this.isMaxLine: false,
      this.isCaps: false,
      this.lineColor,
      this.prefixIcon,
       required this.validator,
     // required this.onSaved,
      this.textEditingController,
      this.enabled: true,
      this.isPassword: false,
      this.isEmail: false,
      this.focusTo,
      this.textColor,
      this.labelColor,
      this.isMobile: false,
      this.prefixImagePath,
      this.isBox: false,
      this.isLabel: false,
      this.iconColor,
      this.textPadding: 5.0,
      this.errorColor,
      this.isOnlyLetters: false});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return new Theme(
        data: theme.copyWith(primaryColor: lineColor ?? white),
        child: new TextFormField(
          enabled: enabled,
          initialValue: initialValue ?? null,
          style: new TextStyle(
              color: textColor ?? white, fontSize: fontSize ?? fontSizeMedium),
          maxLength: lengthSize,
          autocorrect: false,
          controller: textEditingController,
          focusNode: focusNode,
          maxLines: isMaxLine ? null : 1,
          keyboardType: isEmail
              ? TextInputType.emailAddress
              : isMobile
                  ? TextInputType.phone
                  : isDate
                      ? TextInputType.datetime
                      : isNumber
                          ? TextInputType.number
                          : TextInputType.multiline,
          textInputAction: isNewLine
              ? TextInputAction.newline
              : isDone
                  ? TextInputAction.done
                  : TextInputAction.next,
         // onSaved: onSaved,
          obscureText: isPassword,
          validator: validator,
          inputFormatters: isNumber
              ? [WhitelistingTextInputFormatter.digitsOnly]
              : isOnlyLetters
                  ? [WhitelistingTextInputFormatter(new RegExp('^[a-zA-Z -]*'))]
                  : null,
          textCapitalization:
              isCaps ? TextCapitalization.sentences : TextCapitalization.none,
          decoration: new InputDecoration(
            labelText: fieldName,
            errorBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(1.2)),
                borderSide: BorderSide(color: errorColor ?? red, width: 0.8)),
            errorStyle: new TextStyle(
                color: errorColor ?? red, fontSize: fontSizeSmall),
            errorMaxLines: 5,
            focusedBorder: isBox
                ? OutlineInputBorder(
                    borderSide:
                        BorderSide(color: lineColor ?? white, width: 1.2),
                    borderRadius: BorderRadius.circular(radius ?? 4.0),
                    gapPadding: 1.0)
                : UnderlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(1.2)),
                    borderSide:
                        BorderSide(color: lineColor ?? white, width: 0.8)),
            enabledBorder: isBox
                ? OutlineInputBorder(
                    borderSide:
                        BorderSide(color: lineColor ?? white, width: 1.0),
                    borderRadius: BorderRadius.circular(radius ?? 4.0),
                    gapPadding: 1.0,
                  )
                : UnderlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    borderSide:
                        BorderSide(color: lineColor ?? white, width: 0.5)),
            labelStyle: new TextStyle(
                color: labelColor ?? white,
                fontSize: fontSize ?? fontSizeMedium),
            counterText: "",
            counterStyle:
                new TextStyle(color: Colors.transparent, fontSize: 0.0),
            hintText: hint,
            hintStyle: new TextStyle(
                color: textColor ?? white,
                fontSize: fontSize ?? fontSizeMedium),
            contentPadding: EdgeInsets.only(
                left: 0.0, right: 0.0, top: textPadding, bottom: textPadding),
            enabled: enabled,
            border: isBox
                ? OutlineInputBorder(
                    borderRadius: BorderRadius.circular(radius ?? 4.0),
                    gapPadding: 1.0,
                    borderSide:
                        BorderSide(color: lineColor ?? white, width: 0.5))
                : UnderlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    borderSide:
                        BorderSide(color: lineColor ?? white, width: 0.5)),
            prefixText: prefixText ?? null,
            prefixIcon: prefixIcon != null
                ? new Padding(
                    padding: EdgeInsetsDirectional.only(
                        start: 0.0,
                        end: 0.0,
                        top: textPadding,
                        bottom: textPadding),
                    child: Icon(
                      prefixIcon,
                      size: fontSizeLarge,
                      color: iconColor ?? white,
                    ),
                  )
                : prefixWidget != null
                    ? prefixWidget
                    : null,
            suffixIcon: sufixIcon != null
                ? new Padding(
                    padding: EdgeInsetsDirectional.only(
                        start: 0.0,
                        end: 0.0,
                        top: textPadding,
                        bottom: textPadding),
                    child: Icon(
                      sufixIcon,
                      size: fontSizeLarge,
                      color: iconColor ?? white,
                    ),
                  )
                : sufixWidget != null
                    ? sufixWidget
                    : null,
          ),
          keyboardAppearance: numberOnly,
          onFieldSubmitted: (String value) {
            FocusScope.of(context).requestFocus(focusTo);
          },
        )); //Material App
  }
}

class ButtonWidget extends StatefulWidget {
  Function buttonCallback;
  String text;
  Color? textColor;
  Color? bgColor;
  BuildContext? context;
  bool isFullWidth;
  double? width;
  double? height;
  bool isBold;
  double roundRadius;
  bool border;
  Color? borderColor;
  bool isAutoWidth;
  bool shadow;
  IconData? sufixIcon;
  double? fontsize;
  EdgeInsetsGeometry? padding;

  ButtonWidget({
    Key? key,
    this.isAutoWidth: false,
    required this.text,
    this.padding,
    this.border: false,
    this.textColor,
    this.bgColor,
    this.shadow: true,
    this.fontsize,
    this.borderColor,
    required this.buttonCallback,
    this.context,
    this.width,
    this.height,
    this.sufixIcon,
    required this.isFullWidth,
    this.roundRadius: 40.0,
    this.isBold: false,
  }) : super(key: key);

  @override
  _ButtonWidgetState createState() => _ButtonWidgetState();
}

class _ButtonWidgetState extends State<ButtonWidget> {
  Widget uiLayout() {
    return new GestureDetector(
      child: new Container(
        width: widget.isFullWidth
            ? double.infinity
            : !widget.isAutoWidth
                ? widget.width
                : null,
        decoration: new BoxDecoration(
            border: widget.border
                ? new Border.all(color: widget.borderColor ?? white)
                : null,
            color: widget.bgColor,
            boxShadow: widget.shadow
                ? [
                    new BoxShadow(
                      color: new Color.fromRGBO(0, 0, 0, 0.3),
                      blurRadius: 10.0,
                    ),
                  ]
                : null,
            borderRadius: new BorderRadius.circular(widget.roundRadius)),
        child: new Padding(
          padding: widget.padding ?? EdgeInsets.all(10.0),
          child: new Text(
            widget.text,
            style: new TextStyle(
                fontSize: widget.fontsize ?? fontSizeButton,
                color: widget.textColor,
                fontWeight:
                    widget.isBold ? FontWeight.bold : FontWeight.normal),
            textAlign: TextAlign.center,
          ),
        ),
      ),
      onTap: () {
        widget.buttonCallback();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return uiLayout();
  }
}
