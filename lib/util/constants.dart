
import 'package:flutter/material.dart';
import 'package:avhshelpline/util/color.dart';

//App
String appName = "AVHS";

//HomeDrawer
String myAccountDrawer = "My Account";
String rateAndShareDrawer = "Rate & Share";
String terms = "Terms & Conditions ";
String logout = "Logout";
String contactUs = "Contact Us";
String about = "About AVHS";
String privacyPolicy = "Privacy Policy";
String dialogueExitApp = "Are you sure you want to exit ?";
String dialogueLogoutApp = "Do you want to logout ?";

//Report
String report = "You have successfully reported this video";

//Map Api key
//var myKey = 'AIzaSyBHOhrbicBRSvgOGoS2489iy17TRr4TP8A';

//Display
double deviceHeight = 0.0;
double deviceWidth = 0.0;

//FontSize
double fontSizeMedium = 0.0;
double fontSizeSmall = 0.0;
double fontSizeLarge = 0.0;
double fontSizeButton = 0.0;
double fontSizeRadio = 0.0;
double fontSizeHyperMed = 0.0;
double fontSizeCardInfoHeader = 0.0;
double fontSizeCardInfoValue = 0.0;
double fontSizeLabel = 0.0;
double fontSizeFloatBut = 0.0;
double playerFrameSize = 0.0;
double playerRoleFrameSize = 0.0;
double playerPitchFrameSize = 0.0;
double homeGroundImageHeight = 0.0;
double playerPointFrameSize = 0.0;

void calculateFontSizeWithDeviceSize(BuildContext context) {
  if (MediaQuery.of(context).orientation == Orientation.portrait) {
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;
    //deviceHeight = 590.0;
    //deviceWidth = 360.0;
    //print(deviceWidth);
  } else {
    deviceHeight = MediaQuery.of(context).size.width;
    deviceWidth = MediaQuery.of(context).size.height;
  }
  fontSizeMedium = deviceWidth / 24.0;
  fontSizeSmall = deviceWidth / 34.2;
  fontSizeLarge = deviceWidth / 17.1;
  fontSizeButton = deviceWidth / 27.4;
  fontSizeRadio = deviceWidth / 36.0;
  fontSizeHyperMed = deviceWidth / 53.0;
  fontSizeCardInfoHeader = deviceWidth / 32.7;
  fontSizeCardInfoValue = deviceWidth / 20.8;
  fontSizeLabel = deviceWidth / 27.4;
  fontSizeFloatBut = deviceWidth / 27.4;
  playerFrameSize = deviceWidth / 5.4;
  playerPointFrameSize = deviceWidth / 7.3;
  playerRoleFrameSize = deviceWidth / 7.3;
  playerPitchFrameSize = deviceWidth / 10.3;
  homeGroundImageHeight = deviceWidth / 1.76;
}

//Theme
buildTheme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    primaryColor: primaryColor,
    primaryColorDark: primaryColorDark,
    primaryTextTheme: _buildTextTheme(base.primaryTextTheme, white),
    primaryIconTheme: base.iconTheme.copyWith(
      color: white,
    ),
    dialogBackgroundColor: white,
    splashColor: primaryColor,
    buttonColor: buttonColor,
    accentColor: primaryColor,
    scaffoldBackgroundColor: scaffoldBG,
    cardColor: white,
    canvasColor: Colors.transparent,
    textSelectionColor: primaryColor,
    errorColor: red,
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
      height: 42.0,
    ),
    textSelectionHandleColor: white,
    accentTextTheme: _buildTextTheme(base.accentTextTheme, white),
    textTheme: _buildTextTheme(base.textTheme, black),
  );
}

_buildTextTheme(TextTheme base, Color color) {
  return base
      .copyWith(
          headline6: base.headline6!.copyWith(fontWeight: FontWeight.w500),
          subtitle1: base.subtitle1!.copyWith(fontSize: 18.0),
          caption: base.caption!
              .copyWith(fontWeight: FontWeight.w400, fontSize: 14.0))
      .apply(displayColor: color, bodyColor: color);
}
