import 'dart:async';
import 'dart:io';
import 'package:avhshelpline/aboutUs/Aboutus.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:avhshelpline/util/save_storage.dart';
import 'package:avhshelpline/util/scaffoldWidgets.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:flutter/cupertino.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  late TabController tabController;
  late TextEditingController searchController;
  late String userId;
  late String userName;
  late String userEmail;
  late String errorMsg;
  bool showSearchIcon = true;
  DateTime currentBackPressTime = DateTime.now();
  late String accessToken;


  @override
  void initState() {
    super.initState();
    save_bool_prefs("isHomeLogin", false);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void profilecall() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => Profile()),
    // );
  }

  void termsCondition() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => TermsCondition()),
    // );
  }

  void policies() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => PrivacyPolicy()),
    // );
  }

  void contact() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => ContactUs()),
    // );
  }

  void aboutus() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => AboutUs()),
    // );
  }

  void logoutBack() {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: new Text("Logout"),
          content: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CustomDialogue(
                isSimpleDialogue: true,
                message:
                "Are you sure you want to logout ?",
              )
            ],
          ),
          actions: <Widget>[
            FlatButton(
              splashColor: primaryColor.withOpacity(0.5),
              child: Text(
                'CANCEL',
                style: TextStyle(color: primaryColor),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              splashColor: primaryColor.withOpacity(0.5),
              child: Text(
                'OK',
                style: TextStyle(color: primaryColor),
              ),
              onPressed: () async {
                // userId = null;
                // SharedPreferences sharedPreferences =
                // await SharedPreferences.getInstance();
                // String log = await save_string_prefs(
                //     "loginuser", null);
                // save_string_prefs("signupuser", null);
                // save_string_prefs("loginuserId", null);
                // save_string_prefs("signUpUesrId", null);
                // save_string_prefs("loginToken", null);
                // save_string_prefs("signUpToken", null);
                // save_string_prefs("emailPref", null);
                // save_string_prefs("passwordPref", null);
                // save_string_prefs(
                //     "emailSignUpPref", null);
                // save_string_prefs(
                //     "passwordSignUpPref", null);
                // clearPrefs();
                Navigator.of(context).pop();
                Navigator.of(context).pushReplacement(
                    new MaterialPageRoute(
                        builder: (context) =>
                        new HomePage()));
              },
            ),
          ],
        ));
  }

  void noExit(){
    Navigator
        .push(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onWillPop,
        child: Theme(
          data: ThemeData(
              primaryIconTheme: IconThemeData(color: Colors.white),
              fontFamily: 'VAG'),
          child: Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "AVHS",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  ),
                  // new Text(
                  //   "A friend in need",
                  //   style: TextStyle(
                  //       color: Colors.white,
                  //       fontWeight: FontWeight.bold,
                  //       fontSize: 12.0),
                  // ),
                ],
              ),
              centerTitle: false,
              backgroundColor: primaryColor,
            ),
            drawer: drawer(),
          ),
        ));
  }

  Widget drawer() {
    return new Drawer(
      child: new Container(
        color: scaffoldBG,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: new Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new GestureDetector(
                        child: Column(
                          children: <Widget>[
                            CircleAvatar(
                                radius: 60.0,
                                backgroundColor: white,
                                backgroundImage:
                                AssetImage('assets/user_image.png')
                              // ThemeChanger.of(context)
                              //     .loginData
                              //     .profile_picture ==
                              //     null
                              //     ? AssetImage('assets/user_image.png')
                              //     : NetworkImage(imageBaseUrl +
                              //     ThemeChanger.of(context)
                              //         .loginData
                              //         .profile_picture),
                            ),
                            new Text(
                              "Varshini",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        onTap: () {
                          // callProfilePage();
                          setState(() {});
                        },
                      )
                    ],
                  )),
              decoration: BoxDecoration(
                color: primaryColor,
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: ListWithDivider(
                title: myAccountDrawer,
                borderWidth: 0.5,
                isNavTile: true,
                textColor: fontColor,
                borderColor: lineColor,
                iconColor: black,
                action: profilecall,
                //icon: Norwood_Icons.my_account,
                subtitle: '',
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: ListWithDivider(
                title: about,
                borderWidth: 0.5,
                textColor: fontColor,
                borderColor: lineColor,
                iconColor: black,
                isNavTile: true,
                action: aboutus,
                //  icon: Norwood_Icons.about_us,
                subtitle: '',
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: ListWithDivider(
                title: privacyPolicy,
                borderWidth: 0.5,
                textColor: fontColor,
                borderColor: lineColor,
                iconColor: black,
                isNavTile: true,
                //icon: Norwood_Icons.contact,
                action: policies,
                subtitle: '',
                //action: registrationSuccessful,
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: ListWithDivider(
                title: terms,
                borderWidth: 0.5,
                textColor: fontColor,
                borderColor: lineColor,
                iconColor: black,
                isNavTile: true,
                action: termsCondition,
                // icon: Norwood_Icons.terms_conditions,
                subtitle: '',
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: ListWithDivider(
                title: contactUs,
                borderWidth: 0.5,
                textColor: fontColor,
                borderColor: lineColor,
                iconColor: black,
                isNavTile: true,
                //icon: Norwood_Icons.contact,
                action: contact,
                subtitle: '',
                //action: registrationSuccessful,
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: ListWithDivider(
                title: logout,
                borderWidth: 0.5,
                textColor: fontColor,
                borderColor: lineColor,
                iconColor: black,
                isNavTile: true,
                //action: logoutClick,
                // icon: Norwood_Icons.logout,
                action: logoutBack,
                subtitle: '',
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<bool> onWillPop() async {
    DateTime now = DateTime.now();
    if (now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Toast.show("Again Tab to exit", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      return Future.value(false);
    }
    exit(0);
    return Future.value(true);
  }
}