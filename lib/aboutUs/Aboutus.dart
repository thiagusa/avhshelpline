import 'package:flutter/material.dart';

class Aboutus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
      ),
      body: new ListView(
        children: <Widget>[
          new Padding(
              padding: EdgeInsets.only(
                  left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
              child: Card(
                  child: new Padding(
                padding: EdgeInsets.only(
                    left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
                child: Text(
                    "Dr Navaneetha Sasikumar,\nMD(Pediatrics), DM( Cardiology, LTMGH, Sion, Mumbai)\nFellowship in Paediatric, Adult Congenital and Structural Heart Disease Interventions\n(University of Toronto, Canada)\nConsultant Paediatric Cardiologist ,\nGovernment Medical College,\nKottayam, Kerala, India.\nPIN 686008\nContact number : +91 9706048664\nE mail : drnavni@yahoo.com\n\nPaediatric Interventional Cardiologist with special interest in Neonatal Interventions, AdultCongenital and Structural Heart Disease Interventions. Underwent focused two years of handson training in catheter / key hole heart operations at two most reputed centres in the world-The Hospital for Sick Children and Toronto General Hospital, under University of Toronto,Toronto, Canada.\n\n\nHave successfully established a paediatric cardiac program in a public sector teaching hospital in India (Government Medical College, Kottayam, Kerala), where specialized care was nonexistent, piggy- backing on existing adult cardiac interventional and surgical program tominimize resource utilization.Fascination for utilizing technology for improving patient care. Recently developed the “smilingheart” app which is targeted at empowering parents , grass root healthcare workers ,Paediatricians and Neonatologists detect congenital heart disease in a timely fashion. Intend tofurther upgrade this to a platform that would help bridge gaps in Paediatric Cardiac Careacross the globe.\\n\nDid Medical Schooling at Tirumala Dewaswam Government Medical College, Allepey, Kerala.Completed MD In Paediatrics From Gandhi Medical College , Bhopal(2006). CompletedTraining in Cardiology (DM) from LTMGH, Sion Mumbai(2009).Worked with an internationally reputed Paediatric Cardiac Care team at Chennai, led bypioneering Paediatric Cardiac Surgeon , Dr KM Cherian. Also worked in various branches ofNarayana Hrudayalaya.Presented research papers in national and international conferences and done publications inreputed journals\n\n\nSkills and Expertise\n\n\n1. Diagnosing and Planning treatment of various heart defects including complex congenitalheart disease in newborns, children and adults\n\n\n2. Performing heart operations through key hole / catheter technology to close holes in theheart, to open valves which are not working, open blood vessels which are closed /narrow / not working properly, change valves without open heart surgery\n\n\n3. Thorough understanding of the managerial and financial aspects of running a successfulcongenital /paediatric cardiac cardiac program , stemming from working in public as well as corporate sector hospitals in India and having a taste of the state funded programs inCanada."),
              )))
        ],
      ),
    );
  }
}
