import 'dart:ui';
import 'package:avhshelpline/login/doctorResponderLogin.dart';
import 'package:avhshelpline/login/usersLogin.dart';
import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginType extends StatefulWidget {
  @override
  _LoginType createState() => _LoginType();
}

class _LoginType extends State<LoginType> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //getAllPermissions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              // height: MediaQuery.of(context).size.height,
              // width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new SizedBox(
                        height: 30.0,
                      ),
                      Image.asset(
                        "assets/avhs.png",
                        height: 270.0,
                        width: 300.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Login Type',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontSize: fontSizeLarge + 40,
                                      // fontFamily: nexaBold
                                    ),
                                  ),
                                  SizedBox(height: 60),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          width: 240,
                                          height: 100,
                                          child: RaisedButton(
                                            // color: isStudentSelected
                                            //     ? blueColor
                                            //     : Colors.white,
                                            color:Color(0xffB1D8B7),
                                            splashColor: Colors.blueGrey,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                            ),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[

                                                Image.asset(
                                                  'assets/avhs.png',
                                                  width: 50,
                                                  height: 50,
                                                ),
                                                //SizedBox(height: 5),
                                                Text(
                                                  'Doctor / Responder',
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(
                                                      // color: isStudentSelected
                                                      //     ? Colors.white
                                                      //     : poweredGreyColor,
                                                      color: white,
                                                      // fontFamily: nexaBold,
                                                      fontSize:
                                                          fontSizeLarge + 20),
                                                )
                                              ],
                                            ),
                                            onPressed: () {
                                              Navigator.push(context, CupertinoPageRoute(builder: (context) => DoctorLogin()));

                                              // setState(() {
                                              //   isStudentSelected = true;
                                              //   isTeacherSelected = false;
                                              //   isParentSelected = false;
                                              //   isBusDriverSelected = false;
                                              //   isAdminSelected = false;
                                              // });
                                              // Navigator.push(
                                              //     context,
                                              //     CupertinoPageRoute(
                                              //         builder: (context) =>
                                              //             LoginScene(
                                              //               isParentSelection:
                                              //               isParentSelected,
                                              //               isTeacherSelection:
                                              //               isTeacherSelected,
                                              //               isBusDriverSelection:
                                              //               isBusDriverSelected,
                                              //               isAdminSelection:
                                              //               isAdminSelected,
                                              //             )));
                                            },
                                          ),
                                        ),
                                        new SizedBox(
                                          height: 30.0,
                                        ),
                                        Container(
                                          width: 240,
                                          height: 100,
                                          child: RaisedButton(
                                            // color: isStudentSelected
                                            //     ? blueColor
                                            //     : Colors.white,
                                            color:Color(0xffB1D8B7),
                                            splashColor: Colors.blueGrey,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                            ),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Image.asset(
                                                  'assets/avhs.png',
                                                  width: 50,
                                                  height: 50,
                                                ),
                                                //SizedBox(height: 5),
                                                Text(
                                                  'Other Users',
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(
                                                      // color: isStudentSelected
                                                      //     ? Colors.white
                                                      //     : poweredGreyColor,
                                                      color: white,
                                                      // fontFamily: nexaBold,
                                                      fontSize:
                                                          fontSizeLarge + 20),
                                                )
                                              ],
                                            ),
                                            onPressed: () {
                                              // setState(() {
                                              //   isStudentSelected = true;
                                              //   isTeacherSelected = false;
                                              //   isParentSelected = false;
                                              //   isBusDriverSelected = false;
                                              //   isAdminSelected = false;
                                              // });
                                              Navigator.push(context, CupertinoPageRoute(builder: (context) => UserLogin()));
                                              // Navigator.push(
                                              //     context,
                                              //     CupertinoPageRoute(
                                              //         builder: (context) =>
                                              //             LoginScene(
                                              //               isParentSelection:
                                              //               isParentSelected,
                                              //               isTeacherSelection:
                                              //               isTeacherSelected,
                                              //               isBusDriverSelection:
                                              //               isBusDriverSelected,
                                              //               isAdminSelection:
                                              //               isAdminSelected,
                                              //             )));
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  )
                                ]),
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
