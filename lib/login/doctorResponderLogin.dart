import 'dart:convert';
import 'dart:io';

import 'package:avhshelpline/util/color.dart';
import 'package:avhshelpline/util/constants.dart';
import 'package:avhshelpline/util/formWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DoctorLogin extends StatefulWidget {
  BuildContext? baseContext;

  DoctorLogin({
    Key? key,
    this.baseContext,
  }) : super(key: key);

  @override
  _DoctorLoginState createState() => _DoctorLoginState();
}

class _DoctorLoginState extends State<DoctorLogin> {
  final mobileNumberController = TextEditingController();
  final passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode mobileNumberFocusNode = new FocusNode();
  final FocusNode passwordFocusNode = new FocusNode();

  bool isLoading = false;

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    print("Login Page");
  }

  Future loginClick() async {
    // await checkNetworkStatus().then((isNetworkAvailabe) async {
    //   if (isNetworkAvailabe) {
    //     if (_formKey.currentState.validate()) {
    //       if (emailController.text.trim() != "" &&
    //           passwordController.text.trim() != "") {
    //         setState(() {
    //           isLoading = !isLoading;
    //         });
    //
    //         loginApi(
    //           email: emailController.text.trim(),
    //           password: passwordController.text.trim(),
    //           //access_code: orgPin
    //         ).then((result) async {
    //           if (result.status_code == "200") {
    //             HomeProvider.of(context).loginData = result;
    //             await save_string_prefs(
    //                 "user_info_by_login", json.encode(result));
    //             uId = result.data.user_details.user_id.toString();
    //             UserId = uId;
    //             isLoading = !isLoading;
    //             Navigator.of(context).pop(true);
    //           } else {
    //             setState(() {
    //               isLoading = !isLoading;
    //               showInSnackBar(_scaffoldKey, "Incorrect credentials");
    //             });
    //           }
    //         }).catchError((onError) {
    //           setState(() {
    //             isLoading = !isLoading;
    //             showInSnackBar(_scaffoldKey, onError.toString());
    //           });
    //         });
    //       } else {
    //         showInSnackBar(_scaffoldKey, "Fields empty");
    //       }
    //     }
    //   } else {
    //     showInSnackBar(_scaffoldKey, "Network unavailable");
    //   }
    // });
  }

  Widget uiLayout() {
    return Container(
      child: Column(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Column(
              children: <Widget>[
                new Form(
                    key: _formKey,
                    child: new Column(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 25.0),
                          child: new Image.asset(
                              'assets/avhs.png',
                          scale: 5,),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 80.0),
                          child: BoxEditText(
                              fieldName: "Mobile Number",
                              textEditingController: mobileNumberController,
                              fontSize: 15,
                              labelColor: black,
                              textColor: black,
                              focusNode: mobileNumberFocusNode,
                              focusTo: passwordFocusNode,

                              iconColor: black,
                              textPadding: 5.0,
                            //  sufixIcon: JoopopSchool_Icons.mail,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                              isMobile: true,
                              isPassword: false,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Email is missing';
                                }
                              }
                              ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 0.0, top: 10.0),
                          child: BoxEditText(
                              fieldName: "Password",
                              textEditingController: passwordController,
                              labelColor: black,
                              textColor: black,
                              focusNode: passwordFocusNode,
                              focusTo: null,
                              fontSize: 15,
                              iconColor: black,
                              textPadding: 5.0,
                              isEmail: false,
                              isBox: false,
                              isLabel: false,
                              lineColor: black,
                             // sufixIcon: JoopopSchool_Icons.password,
                              isMobile: false,
                              isPassword: true,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Password is missing';
                                }
                              }
                              ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(right: 20.0, top: 30.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new GestureDetector(
                                child: new Text(
                                  'Forgot Password?',
                                  style: new TextStyle(
                                      color: black,
                                      fontSize: 15,
                                      decoration: TextDecoration.underline),
                                ),
                                onTap: () {
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(
                                  //       builder: (context) =>
                                  //           ForgotPasswordEmail()),
                                  // );
                                },
                              )
                            ],
                          ),
                        )
                      ],
                    )),
                new Padding(
                  padding: EdgeInsets.only(
                      left: 40.0, right: 70.0, bottom: 30.0, top: 30.0),
                  child: ButtonWidget(
                    text: "Sign In",
                    isBold: true,
                    textColor: white,
                    bgColor: Color(0xff6CB0A8),
                    buttonCallback: loginClick,
                    context: context,
                    roundRadius: 25.0,
                    isFullWidth: false,
                    isAutoWidth: true,
                    fontsize: 20,
                    padding: new EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0.0,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                "Login",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
              ),
            ],
          ),
          centerTitle: true,
          backgroundColor: Color(0xff94C973),
        ),

        body: new Container(
          width: double.infinity,
          height: double.infinity,
          decoration: new BoxDecoration(
              image: DecorationImage(
                  image: ExactAssetImage('assets/Joopop_assets/Login_bg.jpg'),
                  fit: BoxFit.fill,
                  alignment: Alignment.topCenter)),
          child: new Stack(
            children: <Widget>[
              !isLoading
                  ? new ListView(
                shrinkWrap: false,
                physics: const AlwaysScrollableScrollPhysics(),
                children: <Widget>[uiLayout()],
              )
                  : centeredCircularProgressIndicator(color: primaryColor)
            ],
          ),
        ));
  }
}
